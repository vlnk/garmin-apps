# ConnectIQ Template
⚗️ Boilerplate for development of applications and faces for the Garmin watches made with the [ConnectIQ SDK](https://developer.garmin.com/connect-iq/sdk/).

## References

### Dependencies

1. A configured [docker](https://www.docker.com/) daemon.
2. `openssl` is required to generate developer keys.

These dependencies can be loaded with `nix-shell` or `nix flake` (unless docker).

### Makefile tasks

#### `make build`

Build the docker image that contains:
- the ConnectIQ SDK with version `CONNECTIQ_VERSION`
- the ConnectIQ binaries setup with ![the SDK manager](https://developer.garmin.com/connect-iq/sdk/)

#### `make key`

Generate a **developer key** that should be used to sign all the application pushed on the Garmin store.

It generates two files:
- `developer_key.pem`
- `developer_key.der`

When these files are generated, the recipe won't re-generate new keys again.

#### `make run`

Run the docker image in a shell session with `bash`.

#### `make sdkmanager`

Run the docker image and launch the installed `sdkmanager`.

## Troubleshooting

### _I have no name!_ error
The user ID configured in the Dockerfile doesn't match the one you use when you run the built container. As this user should match yours in your current session, rebuilding the image should fix it.

## Acknowledgements

- [kalemena/docker-connectiq - Garmin Tools - Connect IQ SDK and Eclipse IDE plugins as a Docker container ](https://github.com/kalemena/docker-connectiq)
