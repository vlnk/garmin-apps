# Check at https://developer.garmin.com/downloads/connect-iq/sdks/sdks.json
CONNECTIQ_VERSION ?= 4.1.7-2022-11-21-562b8a195

UID := $(shell id -u)
GID := $(shell id -g)

build:
	docker build \
		--pull \
		--build-arg VERSION=$(CONNECTIQ_VERSION) \
		--build-arg UID=$(UID) \
		-t vlnk/connectiq:latest \
		-t vlnk/connectiq:$(CONNECTIQ_VERSION) .

key:
ifeq (,$(wildcard developer_key.der))
	$(info "Generating Certificate keys")
	openssl genrsa \
		-out developer_key.pem 4096
	openssl pkcs8 \
		-topk8 \
		-inform PEM \
		-outform DER \
		-in developer_key.pem \
		-out developer_key.der -nocrypt
else
	$(warning "Certificate keys already created")
endif

define docker_run
	docker run -it --rm \
		-v $(shell pwd)/Garmin:/home/connectIQ/.Garmin \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-e DISPLAY=unix$(DISPLAY) \
		-u $(UID):$(GID) \
		--privileged \
		vlnk/connectiq:$(CONNECTIQ_VERSION) $(1)
endef

run:
	$(call docker_run, bash)

sdkmanager:
	$(call docker_run, sdkmanager)
