FROM ubuntu:22.04 AS dependencies

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
ARG ADDITIONAL_PACKAGES
ARG UID

ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# Compiler tools
RUN apt update -y \
 && apt install --no-install-recommends -qqy \
    binutils \
    unzip \
    wget \
    curl \
    git \
    ssh \
    tar \
    gzip \
    tzdata \
    ca-certificates \
    gnupg2 \
    libusb-dev \
    libpng-dev \
    libgtk2.0-dev \
    libgtk-3-dev \
    libwebkit2gtk-4.0-dev \
 && apt clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

FROM dependencies AS connectiq-sdk

ARG VERSION

# Check at https://developer.garmin.com/downloads/connect-iq/sdks/sdks.json
#          https://developer.garmin.com/downloads/connect-iq/sdk-manager/sdk-manager.json
ENV CONNECT_IQ_SDK_URL https://developer.garmin.com/downloads/connect-iq

# (optional) if .Garmin folder is mounted at runtime, SDK install is not required
RUN echo "Downloading Connect IQ SDK: ${VERSION}" \
 && cd /opt \
 && curl -LsS -o ciq.zip ${CONNECT_IQ_SDK_URL}/sdks/connectiq-sdk-lin-${VERSION}.zip \
 && unzip ciq.zip -d ciq \
 && rm -f ciq.zip

RUN echo "Downloading Connect IQ SDK Manager:" \
 && cd /opt \
 && curl -LsS -o ciq-sdk-manager.zip ${CONNECT_IQ_SDK_URL}/sdk-manager/connectiq-sdk-manager-linux.zip \
 && unzip ciq-sdk-manager.zip -d ciq \
 && rm -f ciq-sdk-manager.zip

FROM connectiq-sdk AS runner
ARG UID

RUN echo "Configure user connectIQ as $UID" \
 && mkdir -p /home/connectIQ \
 && echo "connectIQ:x:$UID:$UID:connectIQ,,,:/home/connectIQ:/bin/bash" >> /etc/passwd \
 && chown -R $UID:0 /opt && chmod -R ug+rw /opt

USER connectIQ
ENV HOME /home/connectIQ
WORKDIR /home/connectIQ

ENV CIQ_HOME /opt/ciq
ENV PATH ${PATH}:${CIQ_HOME}/bin
